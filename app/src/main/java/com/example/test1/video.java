package com.example.test1;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class video extends AppCompatActivity {


    VideoView mvideoview;
    MediaController mediaController;
    String videopath;
    GlobalVariable gv;
    Uri videoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);


        videopath = "android.resource://com.example.test1/"+R.raw.howfu;
        videoUri = Uri.parse(videopath);



        mediaController = new MediaController(this);

        mvideoview = (VideoView)findViewById(R.id.video);

        mvideoview.setVideoURI(videoUri);
        mvideoview.setMediaController(mediaController);
        mediaController.setAnchorView(mvideoview);
        mvideoview.start();

    }
}
