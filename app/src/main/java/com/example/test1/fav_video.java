package com.example.test1;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class fav_video extends AppCompatActivity {

    VideoView mvideoview;
    MediaController mediaController;
    Uri videoUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_video);

        String videopath7 = "android.resource://com.example.test1/"+R.raw.produce;
        videoUri = Uri.parse(videopath7);

        mediaController = new MediaController(this);

        mvideoview = (VideoView)findViewById(R.id.video1);


        mvideoview.setVideoURI(videoUri);
        mvideoview.setMediaController(mediaController);
        mediaController.setAnchorView(mvideoview);
        mvideoview.start();

    }
}
