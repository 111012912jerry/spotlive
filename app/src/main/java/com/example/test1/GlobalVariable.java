package com.example.test1;

import android.app.Application;
import android.net.Uri;

public class GlobalVariable extends Application {

    private Uri uri= null;
    private boolean flag=false;
    //setter
    public void setURI(Uri u){
        this.uri = u;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    //getter
    public Uri getURI(){
        return uri;
    }

    public boolean getFlag() {
        return flag;
    }
}
