package com.example.test1;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Discounts_of_coins extends AppCompatActivity {

    TextView assets;
    ImageView discount_img;
    TextView discount_title;
    TextView needed_coins;
    ImageView coin;
    TextView discount_discription;
    Button confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discounts_of_coins);

        int postion = getIntent().getIntExtra("position",0);

        assets = (TextView) findViewById(R.id.assets);
        discount_img = (ImageView)findViewById(R.id.discount_img);
        discount_title = (TextView) findViewById(R.id.discount_title);
        needed_coins = (TextView) findViewById(R.id.needed_coins);
        coin = (ImageView) findViewById(R.id.discount_coin);
        discount_discription = (TextView) findViewById(R.id.discount_info);
        confirm = (Button) findViewById(R.id.confirm_exchange);

        Bundle bundle = getIntent().getExtras();

        assets.setText("你好，你現在有1000枚硬幣");
        coin.setImageResource(R.drawable.coin);

        switch (postion){
            case 0:
                discount_img.setImageResource(R.drawable.breakfast);
                discount_title.setText("摩斯漢堡 - 經典早餐買一送一");
                needed_coins.setText("2500");
                discount_discription.setText("肥宅歡樂套餐，每天早上7點前領取，包你領不到");
                break;
            case 1:
                discount_img.setImageResource(R.drawable.soupbuns);
                discount_title.setText("來來豆漿店 - 送一份小籠湯包");
                needed_coins.setText("2000");
                discount_discription.setText("感謝吳xx熱情贊助，每天晚上9點到12點免費外送至七舍102");
                break;
            case 2:
                discount_img.setImageResource(R.drawable.milkshop);
                discount_title.setText("迷克夏 - 免費加珍珠");
                needed_coins.setText("1750");
                discount_discription.setText("每日血汗的好夥伴，讓妳喝著熱可可的同時也可以吃到珍珠");
                break;
            case 3:
                discount_img.setImageResource(R.drawable.costco);
                discount_title.setText("Costco - 消費打85折");
                needed_coins.setText("3500");
                discount_discription.setText("妳生活好夥伴，促銷期間與女生同行，全面再打八折，若與男生同行全面多收20元!成就妳/你的好選擇");
                break;
            case 4:
                discount_img.setImageResource(R.drawable.museum);
                discount_title.setText("故宮博物院 - 特展門票乙張");
                needed_coins.setText("6000");
                discount_discription.setText("再活動期間，使用此券免費擁有限量翠玉白菜");
                break;
        }

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Discounts_of_coins.this).setTitle("確認")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("你即將購買這份產品 \n請問您是否同意")
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(),"你已購買了這項東西",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

    }
}
