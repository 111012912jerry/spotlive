package com.example.test1;

public class ListModel {

    private int videoimgid;
    private String videotitle;
    private String videolength;

    public ListModel(int videoimgid, String videotitle, String videolength) {
        this.videoimgid = videoimgid;
        this.videotitle = videotitle;
        this.videolength = videolength;
    }

    //getter
    public int getVideoimgid() {
        return videoimgid;
    }

    public String getVideotitle() {
        return videotitle;
    }

    public String getVideolength() {
        return videolength;
    }

    //setter

    public void setVideoimgid(int videoimgid) {
        this.videoimgid = videoimgid;
    }

    public void setVideotitle(String videotitle) {
        this.videotitle = videotitle;
    }

    public void setVideolength(String videolength) {
        this.videolength = videolength;
    }
}
