package com.example.test1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

import nctu.fintech.appmate.Table;
import nctu.fintech.appmate.Tuple;

public class newactivity extends AppCompatActivity {

    Table activityTable, videoTable;
    EditText actET, couponET;
    TextView date_start_tv, date_end_tv;
    String activity, start_date, end_date, coupon, ad;
    int i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newactivity);

        activityTable = new Table("http://172.20.10.4:8000/api", "activity", "elvachiu", "elva1101");
        videoTable = new Table("http://172.20.10.4:8000/api", "video", "elvachiu", "elva1101");

        actET = (EditText)findViewById(R.id.NAet1);
        couponET = (EditText)findViewById(R.id.NAet3);

        Button choose_date_start = (Button) findViewById(R.id.NAtimebt1);
        date_start_tv = (TextView)findViewById(R.id.NA_date_tv1);
        choose_date_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar= Calendar.getInstance();
                int year=calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day=calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int month, int day){
                        String datetime=String.valueOf(year)+"/"+String.valueOf(month+1)+"/"+String.valueOf(day);
                        date_start_tv.setText(datetime);
                    }
                },year,month,day).show();
            }
        });

        Button choose_date_end = (Button) findViewById(R.id.NAtimebt2);
        date_end_tv = (TextView)findViewById(R.id.NA_date_tv2);
        choose_date_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar= Calendar.getInstance();
                int year=calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day=calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int month, int day){
                        String datetime=String.valueOf(year)+"/"+String.valueOf(month+1)+"/"+String.valueOf(day);
                        date_end_tv.setText(datetime);
                    }
                },year,month,day).show();
            }
        });

        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        final String[] type = {"無需求", "1支", "2支", "3支", "4支", "5支"};
        ArrayAdapter<String> typeList = new ArrayAdapter<>(newactivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                type);
        spinner.setAdapter(typeList);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(newactivity.this, "您選的是" + type[position], Toast.LENGTH_SHORT).show();
                i = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(newactivity.this, "您沒有選擇任何項目", Toast.LENGTH_SHORT).show();
            }
        });

        Button confirm = (Button) findViewById(R.id.NAbt);
        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                activity = actET.getText().toString();
                start_date = date_start_tv.getText().toString();
                end_date = date_end_tv.getText().toString();
                coupon = couponET.getText().toString();
                ad = type[i];
                Thread t1 = new Thread(r1);
                t1.start();
                actET.setText("");
                couponET.setText("");
                Intent intent = new Intent();
                intent.setClass(newactivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
    private Runnable r1 = new Runnable(){
        public void run()
        {
            Tuple tuple_add = new Tuple();
            tuple_add.put("title", activity);
            tuple_add.put("time_start", start_date);
            tuple_add.put("time_end", end_date);
            tuple_add.put("discount", coupon);
            tuple_add.put("ad", ad);
            try {
                activityTable.add(tuple_add);
            }catch (IOException e) {

                Log.e("Error", "Fail to put ");
            }
        }
    };
}
