package com.example.test1;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Discount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);

        TextView name_tv = (TextView)findViewById(R.id.cp_name_tv2);
        TextView time_tv = (TextView)findViewById(R.id.cp_time_tv2);
        TextView discount_tv = (TextView)findViewById(R.id.cp_discount_tv2);
        Button confirm = (Button)findViewById(R.id.confirmact);
        Bundle bundle = getIntent().getExtras();
        String name = bundle.getString("inputs1" );
        String time = bundle.getString("inputs2");
        String discount = bundle.getString("inputs3");
        name_tv.setText(name);
        time_tv.setText(time);
        discount_tv.setText(discount);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(Discount.this).setTitle("確認")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("你即將要送出這個活動\n請問您是否同意\n" +
                                "此舉動將會把這個活動加到地圖上以供別人點閱")
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setClass(Discount.this,MainActivity.class);
                                Double lat = 24.787607;
                                Double lng = 120.999864;
                                Bundle bundle = new Bundle();
                                bundle.putDouble("lat",lat);
                                bundle.putDouble("lng",lng);
                                intent.putExtras(bundle);
                                startActivityForResult(intent,102);
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });
    }
}
