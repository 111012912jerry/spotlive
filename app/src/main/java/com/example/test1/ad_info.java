package com.example.test1;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class ad_info extends AppCompatActivity {

    private static int MAKEVIDEO_REQUEST = 330;
    private Uri videoUri = null;
    FloatingActionButton make_video;
    VideoView mvideoview;
    MediaController mediaController;
    GlobalVariable gv;

    TextView title_tv, video_tv, info_tv1, info_tv2, info_tv3;
    ImageView picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_info);

        title_tv = (TextView)findViewById(R.id.ad_info_tv1);
        video_tv = (TextView)findViewById(R.id.ad_info_tv2);
        info_tv1 = (TextView)findViewById(R.id.ad_info_tv3);
        info_tv2 = (TextView)findViewById(R.id.ad_info_tv4);
        info_tv3 = (TextView)findViewById(R.id.ad_info_tv5);
        picture = (ImageView)findViewById(R.id.ad_info_image);
        mvideoview = (VideoView)findViewById(R.id.video);
        gv = (GlobalVariable)getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        int id = bundle.getInt("id");
         if(id == 0) {
            title_tv.setText("六扇門三人同行一鍋免費");
            video_tv.setText("影片長度: 15~30秒");
            info_tv1.setText("一年四季都適合吃的小火鍋");
            info_tv2.setText("大學生晚餐最佳選擇");
            info_tv3.setText("三人分享最划算");
            picture.setImageResource(R.drawable.coin);
        }
        else if(id == 5){
            title_tv.setText("摩斯漢堡經典早餐買一送一");
            video_tv.setText("影片長度: 15~30秒");
            info_tv1.setText("展現出經典早餐的美味");
            info_tv2.setText("和親朋好友共享美好早晨");
            info_tv3.setText("以及此活動揪甘心的優惠");
            picture.setImageResource(R.drawable.breakfast);
        }
        else if(id == 2){
            title_tv.setText("來來豆漿店送小籠包");
            video_tv.setText("影片長度: 30秒以內");
            info_tv1.setText("一早大排長龍的顧客們");
            info_tv2.setText("眼前剛出爐的美味小籠包");
            info_tv3.setText("呈現幸福有活力的樣子");
            picture.setImageResource(R.drawable.soupbuns);
        }
        else if(id == 3){
            title_tv.setText("迷克夏免費加珍珠");
            video_tv.setText("影片長度: 20秒左右");
            info_tv1.setText("夏季不能不喝的飲料");
            info_tv2.setText("白玉珍珠好吃的口感");
            info_tv3.setText("給消費者最棒的回饋");
            picture.setImageResource(R.drawable.milkshop);
        }
        else if(id == 4){
            title_tv.setText("Costco消費打八五折");
            video_tv.setText("影片長度: 15~30秒");
            info_tv1.setText("大份量最適合多人分享");
            info_tv2.setText("商品多樣又實惠");
            info_tv3.setText("您購物的好選擇");
            picture.setImageResource(R.drawable.costco);
        }
        else if(id == 1){
            title_tv.setText("草地音樂節特展");
            video_tv.setText("影片長度: 30秒左右");
            info_tv1.setText("充滿文藝氣息的音樂祭");
            info_tv2.setText("豐富您的文化涵養");
            info_tv3.setText("休憩的好去處");
            picture.setImageResource(R.drawable.museum);
        }

        make_video = (FloatingActionButton)findViewById(R.id.floatingActionButton);
        make_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent video_intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(video_intent.resolveActivity(getPackageManager())!=null){
                    startActivityForResult(video_intent, MAKEVIDEO_REQUEST);
                }
            }
        });

        mediaController = new MediaController(this);
    }

    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        if(requestCode==MAKEVIDEO_REQUEST && resultCode==RESULT_OK){
            videoUri = data.getData();
            gv.setFlag(true);
            gv.setURI(videoUri);
        }
    }
}