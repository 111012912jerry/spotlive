package com.example.test1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

public class UserInfo extends AppCompatActivity {
    // vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Integer> mImageUrls = new ArrayList<>();
    private ArrayList<Integer> mHeadShot = new ArrayList<>();
    private ArrayList<String> number_of_dollar = new ArrayList<>();
    private ArrayList<String> number_of_follower = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        getImages();
    }

    private void getImages(){
        mHeadShot.add(R.drawable.image1);
        mImageUrls.add(R.drawable.image1_background);
        mNames.add("崇幃");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.image4);
        mImageUrls.add(R.drawable.image4);
        mNames.add("Deru");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.curator);
        mImageUrls.add(R.drawable.curator);
        mNames.add("館長");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.like);
        mImageUrls.add(R.drawable.like);
        mNames.add("理科太太");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.image6);
        mImageUrls.add(R.drawable.image6);
        mNames.add("Howard");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.pewdiepie);
        mImageUrls.add(R.drawable.pewdiepie);
        mNames.add("Pewdiepei");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.image9);
        mImageUrls.add(R.drawable.image9);
        mNames.add("Charley");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.image10);
        mImageUrls.add(R.drawable.image10);
        mNames.add("Audrey");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        mHeadShot.add(R.drawable.howhow);
        mImageUrls.add(R.drawable.howhow);
        mNames.add("Howhow");
        number_of_dollar.add("11");
        number_of_follower.add("213");

        initRecyclerView();
        initRecyclerView_fav_video();
    }

    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter1 adapter = new RecyclerViewAdapter1(this, mNames, mImageUrls, number_of_dollar, number_of_follower);
        recyclerView.setAdapter(adapter);
    }

    private void initRecyclerView_fav_video(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView1= findViewById(R.id.recyclerView_favorite_story);
        recyclerView1.setLayoutManager(layoutManager);
        RecyclerViewAdapter_fav_video adapter_fav_video = new RecyclerViewAdapter_fav_video(this, mNames, mImageUrls, mHeadShot);
        recyclerView1.setAdapter(adapter_fav_video);
    }

}