package com.example.test1;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;


public class Coupon extends AppCompatActivity {

    private RecyclerView recyclerView;
    ArrayList<ModelCoin> coinList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_of_coin);

        recyclerView = findViewById(R.id.rv);

        coinList = new ArrayList<>();
        coinList.add(new ModelCoin(R.drawable.breakfast, "breakfast", "全台通用", "50元"));
        coinList.add(new ModelCoin(R.drawable.soupbuns, "soupbuns", "臺北市信義區市民大道5段50號", "30元"));
        coinList.add(new ModelCoin(R.drawable.costco, "milk", "全台通用", "100元"));
        coinList.add(new ModelCoin(R.drawable.milkshop, "bubble milk tea", "台北市中山區南京西路8號", "第二杯半價"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager rvLiLayoutManger = layoutManager;

        recyclerView.setLayoutManager(rvLiLayoutManger);

        CoinAdapter adapter = new CoinAdapter(this, coinList);
        recyclerView.setAdapter(adapter);
    }


}