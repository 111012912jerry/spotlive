package com.example.test1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class fragment_local extends Fragment {

    private RecyclerView myrecycleview;
    private List<fan_model> lstFanmodel;
    View v;

    public fragment_local() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_local,container,false);

        myrecycleview = (RecyclerView) v.findViewById(R.id.recyclerview_local);
        RecycleViewAdapter recyclerAdapter = new RecycleViewAdapter(getContext(),lstFanmodel);
        myrecycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecycleview.setAdapter(recyclerAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstFanmodel = new ArrayList<>();
        lstFanmodel.add(new fan_model("My Video",R.drawable.image1));
        lstFanmodel.add(new fan_model("蔡阿嘎",R.drawable.ahga));
        lstFanmodel.add(new fan_model("館長",R.drawable.curator));
        lstFanmodel.add(new fan_model("Howhow",R.drawable.howhow));
        lstFanmodel.add(new fan_model("白癡公主",R.drawable.idiot));
        lstFanmodel.add(new fan_model("黃大謙",R.drawable.dachein));
        lstFanmodel.add(new fan_model("理科太太",R.drawable.like));
        lstFanmodel.add(new fan_model("阿滴",R.drawable.raydu));
        lstFanmodel.add(new fan_model("千千進食中",R.drawable.eating));
        lstFanmodel.add(new fan_model("展榮展瑞",R.drawable.twins));
    }
}
