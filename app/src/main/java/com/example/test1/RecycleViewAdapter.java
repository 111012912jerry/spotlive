package com.example.test1;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder> {

    Context mContext;
    List<fan_model> mData;
    GlobalVariable gv;

    public RecycleViewAdapter(Context mContext, List<fan_model> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.customlayout,viewGroup,false);
        final MyViewHolder vHolder = new MyViewHolder(v);
        gv = (GlobalVariable)mContext.getApplicationContext();

        vHolder.item_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                gv.setFlag(true);
                intent.setClass(mContext,video.class);
                mContext.startActivity(intent);
            }
        });

        return vHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {

        myViewHolder.tv_name.setText(mData.get(i).getName());
        myViewHolder.img.setImageResource(mData.get(i).getPhoto());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_name;
        private ImageView img;
        private LinearLayout item_selected;

        public MyViewHolder(View itemView) {
            super(itemView);

            item_selected = (LinearLayout) itemView.findViewById(R.id.customlayout);
            tv_name = (TextView)itemView.findViewById(R.id.name);
            img = (ImageView) itemView.findViewById(R.id.img_contact);
        }
    }
}
