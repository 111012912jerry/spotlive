package com.example.test1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

class RecyclerViewAdapter_fav_video extends RecyclerView.Adapter<RecyclerViewAdapter_fav_video.ViewHolder> {
    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Integer> mImageUrls = new ArrayList<>();
    private ArrayList<Integer> mImageHeadShot = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapter_fav_video(Context context, ArrayList<String> names, ArrayList<Integer> imageUrls, ArrayList<Integer> imageHeadShot) {
        mContext = context;
        mNames = names;
        mImageUrls = imageUrls;
        mImageHeadShot = imageHeadShot;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.userinfo_favorite_video, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Glide.with(mContext)
                .asBitmap()
                .load(mImageHeadShot.get(position))
                .into(holder.head_shot);

        Glide.with(mContext)
                .asBitmap()
                .load(mImageUrls.get(position))
                .into(holder.image);

        holder.name.setText(mNames.get(position));

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,fav_video.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView head_shot;
        TextView name;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            head_shot = itemView.findViewById(R.id.head_shot);
            image = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.name);

        }
    }
}