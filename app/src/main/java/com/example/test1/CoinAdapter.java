package com.example.test1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.ViewHolder> {

    private Context mContext;
    private  ArrayList<ModelCoin>mList;

    CoinAdapter(Context context, ArrayList<ModelCoin>list){
        mContext = context;
        mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.rv_coin_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext).setTitle("兌換")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("是否確定要兌換這個東西?")
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new AlertDialog.Builder(mContext).setTitle("謝謝")
                                        .setIcon(R.mipmap.ic_launcher)
                                        .setMessage("恭喜你兌換成功\n" +
                                                    "歡迎下次再來消費").show();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        ModelCoin coinItem = mList.get(i);
        ImageView image = viewHolder.item_image;
        TextView  name, place, price;

        name = viewHolder.item_name;
        place = viewHolder.item_place;
        price = viewHolder.item_price;

        image.setImageResource(coinItem.getImage());

        name.setText(coinItem.getName());
        place.setText(coinItem.getPlace());
        price.setText(coinItem.getPrice());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView item_image;
        TextView item_name, item_place, item_price;

        public ViewHolder(View itemView){
            super(itemView);

            item_image = itemView.findViewById(R.id.item_image);
            item_name = itemView.findViewById(R.id.item_name);
            item_place =itemView.findViewById(R.id.item_place);
            item_price = itemView.findViewById(R.id.item_price);

        }

    }

}