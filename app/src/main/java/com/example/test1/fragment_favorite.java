package com.example.test1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class fragment_favorite extends Fragment {

    View v;
    private RecyclerView myrecycleview;
    private List<fan_model> lstFanmodel;

    public fragment_favorite() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_favorite,container,false);

        myrecycleview = (RecyclerView) v.findViewById(R.id.recyclerview_favorite);
        RecycleViewAdapter recyclerAdapter = new RecycleViewAdapter(getContext(),lstFanmodel);
        myrecycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecycleview.setAdapter(recyclerAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("oncreate","start");
        lstFanmodel = new ArrayList<>();
        lstFanmodel.add(new fan_model("My Video",R.drawable.image1));
        lstFanmodel.add(new fan_model("Jimmy",R.drawable.image2));
        lstFanmodel.add(new fan_model("Vivian",R.drawable.image3));
        lstFanmodel.add(new fan_model("Deru",R.drawable.image4));
        lstFanmodel.add(new fan_model("Elva",R.drawable.image5));
        lstFanmodel.add(new fan_model("Howard",R.drawable.image6));
        lstFanmodel.add(new fan_model("Allen",R.drawable.image7));
        lstFanmodel.add(new fan_model("Ash",R.drawable.image8));
        lstFanmodel.add(new fan_model("Charley",R.drawable.image9));
        lstFanmodel.add(new fan_model("Audrey",R.drawable.image10));

        Log.e("oncreate","finish");

    }
}
