package com.example.test1;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;


public class Government extends AppCompatActivity {

    private RecyclerView recyclerView;
    ArrayList<ModelCoin> coinList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_of_coin);

        recyclerView = findViewById(R.id.rv);

        coinList = new ArrayList<>();
        coinList.add(new ModelCoin(R.drawable.find, "發現台灣", "陽明海洋文化藝術館 第一展覽室", "雙人套票"));
        coinList.add(new ModelCoin(R.drawable.railway, "鐵道博物館", "台北市中山區南京西路8號", "7折券"));
        coinList.add(new ModelCoin(R.drawable.tpe, "台北藝文祭", "朱銘美術館", "單人免費券"));
        coinList.add(new ModelCoin(R.drawable.grassmusic, "草地音樂季", "花蓮東大門夜市陽光電城草地", "活動券50元"));
        coinList.add(new ModelCoin(R.drawable.music,"亞太理事音樂會","台中新烏日國際展覽館","9折券"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager rvLiLayoutManger = layoutManager;

        recyclerView.setLayoutManager(rvLiLayoutManger);

        CoinAdapter adapter = new CoinAdapter(this, coinList);
        recyclerView.setAdapter(adapter);
    }


}