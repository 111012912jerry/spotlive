package com.example.test1;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import nctu.fintech.appmate.Table;
import nctu.fintech.appmate.Tuple;

public class makevideo extends AppCompatActivity {

    Table activityTable;
    String name, vid;
    ImageView invisible;
    boolean flag=true;

    private ListView listView;
    String[] title, ad, title2, ad2;
    List<HashMap<String , String>> list = new ArrayList<>();
    List<HashMap<String , String>> list2 = new ArrayList<>();
    public ListAdapter listAdapter, listAdapter2;

    private TimerTask task = new TimerTask(){
        public void run() {
            Thread t2 = new Thread(r2);
            t2.start();
        }
    };

    private Runnable r2 = new Runnable(){
        public void run()
        {
            try {
                Tuple tuple_get[] = activityTable.get();
                name = "";
                vid = "";
                for(int i=0; i<tuple_get.length; i++){
                    String tempString = tuple_get[i].get("title");
                    if(tempString!=null && !tempString.equals("")) {
                        title[5] = tempString  + "\n" + name;
                    }
                }
                //Toast.makeText(makevideo.this, name, Toast.LENGTH_SHORT).show();

                for(int i=0; i<tuple_get.length; i++){
                    String tempString = tuple_get[i].get("ad");
                    if(tempString!=null && !tempString.equals("")) {
                        ad[5] = "宣傳影片: " + tempString  + "\n" + vid;
                    }
                }
                //Toast.makeText(makevideo.this, vid, Toast.LENGTH_SHORT).show();

                Message msg = new Message();
                msg.what = 1;
                mHandler.sendMessage(msg);
            }catch (IOException e){
                Log.e("Net", "Fail to get");
            }
        }
    };

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    listView.setAdapter(listAdapter);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makevideo);

        activityTable = new Table("http://172.20.10.4:8000/api", "activity", "elvachiu", "elva1101");

        title = new String[]{"草地音樂祭展覽" , "來來豆漿店送小籠包" , "摩斯漢堡經典早餐買一送一" , "Costco消費打八五折", "迷克夏免費加珍珠"};
        ad = new String[]{"宣傳影片: 1支" , "宣傳影片: 1支" , "宣傳影片: 2支" , "宣傳影片: 2支", "宣傳影片:1支"};
        title2 = new String[]{"六扇門三人同行送一鍋", "草地音樂祭展覽" , "來來豆漿店送小籠包" , "摩斯漢堡經典早餐買一送一" , "Costco消費打八五折", "迷克夏免費加珍珠"};
        ad2 = new String[]{"宣傳影片: 1支", "宣傳影片: 1支" , "宣傳影片: 1支" , "宣傳影片: 2支" , "宣傳影片: 2支", "宣傳影片: 1支"};
        listView = (ListView) findViewById(R.id.listView);

        Timer timer01 = new Timer();
        //timer01.schedule(task, 0, 5000);

        //使用List存入HashMap，用來顯示ListView上面的文字。
        for(int i = 0 ; i < title.length ; i++){
            HashMap<String , String> hashMap = new HashMap<>();
            hashMap.put("title" , title[i]);
            hashMap.put("ad" , ad[i]);
            //把title , text存入HashMap之中
            list.add(hashMap);
            //把HashMap存入list之中
        }
        // 5個參數 : context , List , layout , key1 & key2 , text1 & text2

        final ListAdapter listAdapter = new SimpleAdapter(
                getApplicationContext(),
                list,
                android.R.layout.simple_list_item_2 ,
                new String[]{"title" , "ad"},
                new int[]{android.R.id.text1 , android.R.id.text2});

        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle id_bundle = new Bundle();
                id_bundle.putInt("id", position);
                Intent intent = new Intent();
                intent.setClass(makevideo.this, ad_info.class);
                intent.putExtras(id_bundle);
                startActivity(intent);
            }
        });

        invisible = (ImageView) findViewById(R.id.invisible);
        invisible.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(flag) {
                    for (int i = 0; i < 6; i++) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("title2", title2[i]);
                        hashMap.put("ad2", ad2[i]);
                        //把title , text存入HashMap之中
                        list2.add(hashMap);
                        //把HashMap存入list之中
                    }
                    listAdapter2 = new SimpleAdapter(
                            getApplicationContext(),
                            list2,
                            android.R.layout.simple_list_item_2,
                            new String[]{"title2", "ad2"},
                            new int[]{android.R.id.text1, android.R.id.text2});
                    //listView.setAdapter(listAdapter);
                    listView.setAdapter(listAdapter2);
                    flag=false;
                }
            }
        });
    }
}