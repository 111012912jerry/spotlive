package com.example.test1;

import android.Manifest;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private static int VIDEO_REQUEST = 101;
    private static int FAN_REQUEST = 102;
    Uri videoUri1 = null;
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1;
    //設定超過一個距離(尺)他就會做更新location的動作
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000;
    //設定超過一個時間(毫秒)他就會做更新location的動作

    LocationManager locationManager;
    LocationListener locationListener;
    private BluetoothAdapter mBluetoothAdapter;
    private List<String> bluetoothdeviceslist = new ArrayList<String>();
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    double dis;
    TextView tv1,save;
    GoogleMap map;

    GlobalVariable globalVariable;

    Uri  vidoeURI1;
    private ClusterManager mClusterManager;
    private MyClusterManagerRenderer mClustermanagerRenderer;
    private ArrayList<ClusterMarker> mClusterMarkers = new ArrayList<>();
    Dialog vidoeDialog;
    VideoView popupvideo;
    ImageView closebt, back;
    ImageView invisible_coin;
    String CHANNEL_ID = "test1.channel1";
    ImageView invis;

    private static final String TAG = "MainActivity";

    private ListView lIstview;
    private ArrayList<ListModel> models;
    private ListAdapter listAdapter;
    private PopupWindow popupWindow;
    private LayoutInflater layoutInflater;
    private ConstraintLayout constraintLayout;

    //vars
    private ArrayList<String>mNames = new ArrayList<>();
    private ArrayList<String>mImageUrls = new ArrayList<>();

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "name";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vidoeDialog = new Dialog(this);
        constraintLayout = findViewById(R.id.main);
        back = findViewById(R.id.back);
        popupvideo = findViewById(R.id.invisvideo);
        save = findViewById(R.id.save);

        globalVariable = (GlobalVariable)getApplicationContext();
        if(globalVariable.getFlag()){
            videoUri1 = globalVariable.getURI();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        invis = (ImageView) findViewById(R.id.invis);
        invis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMapMarker(1);
            }
        });

        final NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        invisible_coin = (ImageView) findViewById(R.id.imageView3);
        invisible_coin.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void onClick(View view) {
                createNotificationChannel();
                Intent notifyIntent = new Intent(MainActivity.this, MainActivity.class);
                notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent appIntent = PendingIntent.getActivity(MainActivity.this, 0, notifyIntent, 0);
                Notification notification = new Notification.Builder(MainActivity.this, CHANNEL_ID)
//                        .setContentIntent(appIntent) // 傳入 Step2 所設定的 PendingIntent
                        .setSmallIcon(R.drawable.ic_menu_send) // 設置狀態列裡面的圖示（小圖示）　　
                        .setLargeIcon(BitmapFactory.decodeResource(MainActivity.this.getResources(), R.drawable.coin)) // 下拉下拉清單裡面的圖示（大圖示）
                        .setContentTitle("成功掃到店家招牌") // 設置下拉清單裡的標題
                        .setContentText("恭喜您獲得10枚硬幣")// 設置上下文內容.setDefaults(Notification.DEFAULT_ALL) //使用所有默認值，比如聲音，震動，閃屏等等
                        .build();

                //把指定ID(常數0)的通知持久的發送到狀態條上.
                mNotificationManager.notify(0, notification);
                Log.e("notification", "set");
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String package_name = "com.project.ar";
                PackageManager packageManager = getPackageManager();
                Intent intent = packageManager.getLaunchIntentForPackage(package_name);
                startActivity(intent);
            }
        });

        tv1 = (TextView) findViewById(R.id.textView);


        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.startDiscovery();
        //checkBluetoothPermission();
        //SearchBluetooth();
        Log.e("testing", "testing");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //如果沒有授權使用定位就會跳出來這個
            // TODO: Consider calling

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 100, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    addMapMarker(0);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.021661, 121.535190), 18));
                    double latitude = location.getLatitude();
                    double longtitude = location.getLongitude();

                    LatLng latLng = new LatLng(latitude, longtitude);
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        List <Address> addressList = geocoder.getFromLocation(latitude, longtitude, 1);
                        String str = addressList.get(0).getLocality() + ",";
                        str += addressList.get(0).getCountryName();
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title(str));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    addMapMarker(0);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
        else if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 100, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    addMapMarker(0);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.021661, 121.535190), 18));
//                    double latitude = location.getLatitude();
//                    double longtitude = location.getLongitude();
//
//                    LatLng latLng = new LatLng(latitude, longtitude);
//                    Geocoder geocoder = new Geocoder(getApplicationContext());
//                    try {
//                        List <Address> addressList = geocoder.getFromLocation(latitude, longtitude, 1);
//                        String str = addressList.get(0).getLocality() + ",";
//                        str += addressList.get(0).getCountryName();
//                        map.clear();
//                        map.addMarker(new MarkerOptions().position(latLng).title(str));
//                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
            addMapMarker(0);
        }

    }

    private void checkBluetoothPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Android M Permission Check
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            }
        }
    }

    public void SearchBluetooth() {
        if (mBluetoothAdapter == null) { //沒找到
            Toast.makeText(this, "not find the bluetooth", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!mBluetoothAdapter.isEnabled()) {
            //藍芽未開 跳出視窗提示使用者是否開啟藍芽
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, 1);
            Set<BluetoothDevice> myDevices = mBluetoothAdapter.getBondedDevices();
            if (myDevices.size() > 0) {
                for (BluetoothDevice device : myDevices)
                    bluetoothdeviceslist.add(device.getName() + ":" + device.getAddress() + "\n"); //藍芽連接的裝置資訊
            }
        }
        //註冊BroadcastReceiver: 用來接收搜尋到的結果
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(myreceiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(myreceiver, filter);
    }

    private final BroadcastReceiver myreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //收到的廣播類型
            String action = intent.getAction();
            //發現設備的廣播
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //從intent中獲取設備
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                double txPower = -59;
                double ratio = rssi * 1.0 / txPower;
                if (ratio < 1.0) {
                    dis = Math.pow(ratio, 10);
                } else {
                    dis = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
                }
                try {
                    if (device.getName().equals("BR517485")) {
                        Log.e("bluetooth","found");
                        new AlertDialog.Builder(MainActivity.this).setTitle("搜尋店家")
                                .setIcon(R.mipmap.ic_launcher)
                                .setMessage("店名：摩斯漢堡　\n" +
                                        "最近優惠： ")
                                .setPositiveButton("前往", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent();
                                        intent.setClass(MainActivity.this, Coupon.class);
                                        Bundle bundle = new Bundle();
                                        intent.putExtras(bundle);
                                        startActivityForResult(intent, 2);
                                    }
                                })
                                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }
                } catch (Exception e) {
                }

            }
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.char_setting) {
            // Handle the camera action
            Intent char_intent = new Intent();
            char_intent.setClass(MainActivity.this, UserInfo.class);
            Bundle bundle = new Bundle();
            char_intent.putExtras(bundle);
            startActivityForResult(char_intent, 1);
        } else if (id == R.id.coupons) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, Coupon.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent, 2);

        } else if (id == R.id.gov_activities) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, Government.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent,3);
        }
        else if (id == R.id.yes) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, newactivity.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent, 4);
        }else if(id == R.id.makevideo){
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, makevideo.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent,6);
        }else if (id == R.id.fans) {
            Log.e("Intent","tofans");
            Intent intent = new Intent();
            intent.setClass(MainActivity.this,fans.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent,101);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        addMapMarker(0);
        map.setOnInfoWindowClickListener(this);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.021661, 121.535190), 18));
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitude = location.getLatitude();
                double longtitude = location.getLongitude();

                LatLng latLng = new LatLng(latitude, longtitude);
                Geocoder geocoder = new Geocoder(getApplicationContext());
                try {
                    List <Address> addressList = geocoder.getFromLocation(latitude, longtitude, 1);
                    String str = addressList.get(0).getLocality() + ",";
                    str += addressList.get(0).getCountryName();
                    map.clear();
                    map.addMarker(new MarkerOptions().position(latLng).title(str));
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                addMapMarker(0);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

    }

    private void addMapMarker(int code){
        map.clear();
        if(map != null){
            if(mClusterManager == null){
                mClusterManager = new ClusterManager<ClusterMarker>(getApplicationContext(),map);
            }
            if(mClustermanagerRenderer==null){
                mClustermanagerRenderer = new MyClusterManagerRenderer(getApplicationContext(),map,mClusterManager);
                mClusterManager.setRenderer(mClustermanagerRenderer);
            }
            //在這裡加入新的marker
            try{
                //可加入不同的文字內容
                String snippet = "Hi";
                int avatar = R.drawable.image1;
                ClusterMarker newClusterMarker = new ClusterMarker(
                        new LatLng(25.021641, 121.535263),
                        "Me",
                        snippet,
                        avatar
                );

                ClusterMarker newClusterMarker2 = new ClusterMarker(
                        new LatLng(25.021, 121.532020),
                        "演唱會",
                        "18:30開始",
                        R.drawable.concert
                );

                ClusterMarker newClusterMarker3 = new ClusterMarker(
                        new LatLng(25.019006, 121.533407),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );

                  ClusterMarker newClusterMarker4 = new ClusterMarker(
                            new LatLng(25.022341, 121.540391),
                            "麵包店",
                            "幫忙趴舍廣告",
                            R.drawable.bread
                    );

                ClusterMarker newClusterMarker5 = new ClusterMarker(
                        new LatLng(25.024042, 121.536905),
                        "名人演講",
                        "18:00入場",
                        R.drawable.product
                );

                ClusterMarker newClusterMarker6 = new ClusterMarker(
                        new LatLng(25.023539, 121.543780),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );

                ClusterMarker newClusterMarker7 = new ClusterMarker(
                        new LatLng(25.025, 121.53506),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );

                ClusterMarker newClusterMarker8 = new ClusterMarker(
                        new LatLng(25.020159, 121.528555),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );

                ClusterMarker newClusterMarker9 = new ClusterMarker(
                        new LatLng(25.017981, 121.527482),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker10 = new ClusterMarker(
                        new LatLng(25.015803, 121.526710),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker11 = new ClusterMarker(
                        new LatLng(25.010125, 121.533147),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker12 = new ClusterMarker(
                        new LatLng(25.009308, 121.539112),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker13 = new ClusterMarker(
                        new LatLng(25.011253, 121.543275),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker14 = new ClusterMarker(
                        new LatLng(25.012925, 121.544820),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker15 = new ClusterMarker(
                        new LatLng(25.024514, 121.526409),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker16 = new ClusterMarker(
                        new LatLng(25.026497, 121.530357),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker17 = new ClusterMarker(
                        new LatLng(25.024125, 121.521302),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker18 = new ClusterMarker(
                        new LatLng(25.021053, 121.521774),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker19 = new ClusterMarker(
                        new LatLng(25.013625, 121.530443),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );
                ClusterMarker newClusterMarker20 = new ClusterMarker(
                        new LatLng(25.014909, 121.532503),
                        "商店",
                        "正在促銷商品",
                        R.drawable.store
                );

                mClusterManager.addItem(newClusterMarker);
                mClusterManager.addItem(newClusterMarker2);
                mClusterManager.addItem(newClusterMarker3);
                mClusterManager.addItem(newClusterMarker5);
                mClusterManager.addItem(newClusterMarker5);
                mClusterManager.addItem(newClusterMarker6);
                mClusterManager.addItem(newClusterMarker7);
                mClusterManager.addItem(newClusterMarker8);
                mClusterManager.addItem(newClusterMarker9);
                mClusterManager.addItem(newClusterMarker10);
                mClusterManager.addItem(newClusterMarker11);
                mClusterManager.addItem(newClusterMarker12);
                mClusterManager.addItem(newClusterMarker13);
                mClusterManager.addItem(newClusterMarker14);
                mClusterManager.addItem(newClusterMarker15);
                mClusterManager.addItem(newClusterMarker16);
                mClusterManager.addItem(newClusterMarker17);
                mClusterManager.addItem(newClusterMarker18);
                mClusterManager.addItem(newClusterMarker19);
                mClusterManager.addItem(newClusterMarker20);

                if(code==1){
                    mClusterManager.addItem(newClusterMarker4);
                }
                //mClusterMarkers.add(newClusterMarker);

            }catch (NumberFormatException e){
                Log.e("Google Map","Null pointer exception");
            }
        }
        mClusterManager.cluster();

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(marker.getTitle().equals("Me")){
            marker.hideInfoWindow();
        }
        else if(marker.getTitle().equals("麵包店")&& globalVariable.getFlag()==false){
            Log.e("Intent","麵包店");
            Intent intent = new Intent();
            intent.setClass(MainActivity.this,filmforstore.class);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivityForResult(intent,1001);
        }
        else if(marker.getTitle().equals("麵包店")&& globalVariable.getFlag()==true){
            ShowVideoPopup(videoUri1);
            Showlistviewpopup();
        }
        else{
            String videoURI = "android.resource://com.example.test1/"+R.raw.first;
            Uri uri = Uri.parse(videoURI);
            ShowVideoPopup(uri);
            Showlistviewpopup();
        }
    }

    public void Showlistviewpopup(){
        lIstview = findViewById(R.id.lstvideo);
        models = VideoListDetial.gerlist();
        listAdapter = new ListAdapter(MainActivity.this,models);
        lIstview.setAdapter(listAdapter);
        save.setVisibility(View.VISIBLE);
        popupvideo.setVisibility(View.VISIBLE);
        lIstview.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                save.setVisibility(View.INVISIBLE);
                popupvideo.setVisibility(View.INVISIBLE);
                lIstview.setVisibility(View.INVISIBLE);
                back.setVisibility(View.INVISIBLE);
            }
        });

        lIstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String videoURI = "android.resource://com.example.test1/"+R.raw.second;
                Uri uri = Uri.parse(videoURI);
                ShowVideoPopup(uri);
            }
        });
    }

    public void  ShowVideoPopup(Uri uri){

        popupvideo = findViewById(R.id.invisvideo);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "已收藏影片", Toast.LENGTH_SHORT).show();
            }
        });
        popupvideo.setVideoURI(uri);
        popupvideo.start();
    }
}
