package com.example.test1;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ListModel> models;

    public ListAdapter(Context context, ArrayList<ListModel> models) {
        this.context = context;
        this.models = models;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView = View.inflate(context,R.layout.videolist_item,null);
        }
        ImageView images = convertView.findViewById(R.id.videoimg);
        TextView title = convertView.findViewById(R.id.videotv);
        TextView videolgth = convertView.findViewById(R.id.videolength);

        ListModel model = models.get(position);
        images.setImageResource(model.getVideoimgid());
        title.setText(model.getVideotitle());
        videolgth.setText(model.getVideolength());

        return convertView;
    }
}
