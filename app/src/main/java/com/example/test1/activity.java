package com.example.test1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity);

        TextView act_tv = (TextView)findViewById(R.id.actv1);
        TextView time_tv = (TextView)findViewById(R.id.actv2);
        TextView coupon_tv = (TextView)findViewById(R.id.actv3);
        TextView spinner_tv = (TextView)findViewById(R.id.actv4);
        Bundle bundle = getIntent().getExtras();
        String act = bundle.getString("inputs1" );
        String time = bundle.getString("inputs2");
        String coupon = bundle.getString("inputs3");
        String spinner = bundle.getString("spinner");
        act_tv.setText("新活動: " + act);
        time_tv.setText("活動時間: " + time);
        coupon_tv.setText("活動優惠: " + coupon);
        spinner_tv.setText("宣傳影片: " + spinner);

        Button delete = (Button) findViewById(R.id.actbt);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TextView act_tv = (TextView)findViewById(R.id.actv1);
                TextView time_tv = (TextView)findViewById(R.id.actv2);
                TextView coupon_tv = (TextView)findViewById(R.id.actv3);
                TextView spinner_tv = (TextView)findViewById(R.id.actv4);
                act_tv.setText("新活動: ");
                time_tv.setText("活動時間: ");
                coupon_tv.setText("活動優惠: ");
                spinner_tv.setText("宣傳影片: ");
            }
        });

        Button check = (Button) findViewById(R.id.actbt2);
        check.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Bundle bundle = getIntent().getExtras();
                Intent intent = new Intent();
                intent.setClass(activity.this, Discount.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, 20);
            }
        });
    }
}
