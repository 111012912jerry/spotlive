package com.example.test1;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class ClusterMarker implements ClusterItem {

    private LatLng position;
    private String title;
    private String snippit;
    private int iconpicture;

    public ClusterMarker() {
    }

    public ClusterMarker(LatLng position, String title, String snippit, int iconpicture) {
        this.position = position;
        this.title = title;
        this.snippit = snippit;
        this.iconpicture = iconpicture;
    }

    //getter

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public String getSnippit() {
        return snippit;
    }

    public int getIconpicture() {
        return iconpicture;
    }

    //setter


    public void setPosition(LatLng position) {
        this.position = position;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSnippit(String snippit) {
        this.snippit = snippit;
    }

    public void setIconpicture(int iconpicture) {
        this.iconpicture = iconpicture;
    }
}
