package com.example.test1;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class filmforstore extends AppCompatActivity {

    private static int MAKEVIDEO_REQUEST = 330;

    Uri videoUri = null;
    FloatingActionButton make_video;
    VideoView mvideoview;
    MediaController mediaController;
    GlobalVariable gv;
    ImageView button;

    TextView title_tv, video_tv, info_tv1, info_tv2, info_tv3;
    ImageView picture;

    GlobalVariable globalVariable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filmforstore);

        globalVariable = (GlobalVariable)getApplicationContext();

        title_tv = (TextView)findViewById(R.id.ad_info_tv1);
        video_tv = (TextView)findViewById(R.id.ad_info_tv2);
        info_tv1 = (TextView)findViewById(R.id.ad_info_tv3);
        info_tv2 = (TextView)findViewById(R.id.ad_info_tv4);
        info_tv3 = (TextView)findViewById(R.id.ad_info_tv5);
        picture = (ImageView)findViewById(R.id.ad_info_image);
        mvideoview = (VideoView)findViewById(R.id.video);
        gv = (GlobalVariable)getApplicationContext();
        button = (ImageView) findViewById(R.id.backtomain);

        Bundle bundle = getIntent().getExtras();

        title_tv.setText("麵包店限時特價");
        video_tv.setText("影片長度: 15~30秒");
        info_tv1.setText("提供最新鮮的麵包");
        info_tv2.setText("大學生早餐最佳選擇");
        info_tv3.setText("買一送ㄧ");
        picture.setImageResource(R.drawable.coin);

        make_video = (FloatingActionButton)findViewById(R.id.floatingActionButton);
        make_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent video_intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(video_intent.resolveActivity(getPackageManager())!=null){
                    startActivityForResult(video_intent, MAKEVIDEO_REQUEST);
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backtomain(v);
            }
        });
    }

    public void backtomain(View view)
    {
        Intent playIntent = new Intent(this, MainActivity.class);
        //playIntent.putExtra("videoUri1",videoUri.toString());
        //playIntent.putExtra("flag1",true);
        globalVariable.setURI(videoUri);
        globalVariable.setFlag(true);
        setResult(RESULT_OK,playIntent);
        startActivityForResult(playIntent,1001);
    }

    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        if(requestCode==MAKEVIDEO_REQUEST && resultCode==RESULT_OK){
            videoUri = data.getData();
            gv.setFlag(true);
            gv.setURI(videoUri);
        }
    }

}
