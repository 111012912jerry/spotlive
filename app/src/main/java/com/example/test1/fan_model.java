package com.example.test1;

public class fan_model {
    private String Name;
    private int Photo;

    public fan_model() {
    }

    public fan_model(String name, int photo) {
        Name = name;
        Photo = photo;
    }

    //Getter

    public String getName() {
        return Name;
    }

    public int getPhoto() {
        return Photo;
    }

    //Setter

    public void setName(String name) {
        Name = name;
    }

    public void setPhoto(int photo) {
        Photo = photo;
    }
}
