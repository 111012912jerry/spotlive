package com.example.test1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class fragment_foreign extends Fragment {

    View v;
    private RecyclerView myrecycleview;
    private List<fan_model> lstFanmodel;

    public fragment_foreign() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_foreign,container,false);
        myrecycleview = (RecyclerView) v.findViewById(R.id.recyclerview_foreign);
        RecycleViewAdapter recyclerAdapter = new RecycleViewAdapter(getContext(),lstFanmodel);
        myrecycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecycleview.setAdapter(recyclerAdapter);
        return v;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("oncreate","start");
        lstFanmodel = new ArrayList<>();
        lstFanmodel.add(new fan_model("My Video",R.drawable.image1));
        lstFanmodel.add(new fan_model("Jess & Gabriel",R.drawable.jessandgabe));
        lstFanmodel.add(new fan_model("Pewdiepie",R.drawable.pewdiepie));
        lstFanmodel.add(new fan_model("Zoella",R.drawable.zoella));
        lstFanmodel.add(new fan_model("三原慧悟",R.drawable.miharakeigo));
        lstFanmodel.add(new fan_model("Kurt Hugo Schneider",R.drawable.kurt));
        lstFanmodel.add(new fan_model("Aspyn Ovard",R.drawable.aspyn));


        Log.e("oncreate","finish");

    }
}
